/**
 * @author: Diney Wankhede
 * @file: main.c
 *
 */
#include <stdio.h>

/**
 *
 * Unions: Unions are type of structures that can be used when you want to use a
 * variable as different data types. The size allocated for the unions is the size 
 * of data types which requires maximum. Hence we can use less memory and implement
 * muliple data types. If we want to take int, float and character from the user and
 * print out the result by using least amount of memory, we can use unions. 
 * 
 * In our example, the output is completely dependent on the order of the statements we 
 * assign values to the variables. If float is assigned at the last union takes and prints its 
 * value. If we assign value to float variable at the begining and character at the last, 
 * The interger prints ASCII of character and character variable prints fine but the float is corrupted.   
 * This shows that the memory is utilized by the character which we assigned at the last. 
 */

 /**
 * Unions and typecasting: Unions basically does the work of typecasting without stating it explicitly.
 * Unions will hold only a single member at a time. The memory allocated to 
 * union is used up by float variable and hence no allocated memory for int or char. 
 * In type casting, when we type cast float to int, memory allocated is less than that by float 
 * and hence only what 'int' can occupy will be printed, similar to union. Hence, we are basically doing the
 * job of type casting using structure union. 
 *
 *
 */ 
union structrue 
{
  int i;
  float f;
  char c;
  int* p;
}unionObj;

void print();

int main()
{  
 
  unionObj.i = 23;
  unionObj.c = 'D'; 
  unionObj.f = 19.92;  
  unionObj.p = unionObj.i; //This generates warning. Acceptable as we are testing.
 
  print();
  return 0;
}

void print() 
{ 
  printf("Integer : %i\n",unionObj.i);
  printf("Character : %c\n",unionObj.c);  
  printf("Float : %2.2f\n",unionObj.f);
  printf("Int Pointer : %i\n" ,unionObj.p); //This generates warning. Acceptable as we are checking.
}

