#include <stdio.h>

union structure 
{
  int i;
  float f;
  char c;
  int* p;
};

int main()
{
   union structure unionObj;

   unionObj.i = 23;   
   printf("Integer : %d\n",unionObj.i);   

   unionObj.f = 19.92;   
   printf("Float : %2.2f\n",unionObj.f);
  
   unionObj.c = 'D';   
   printf("Character : %c\n",unionObj.c);
   
   unionObj.p = &unionObj.i;
   printf("Int Pointer : %d\n" ,*unionObj.p); 

   return 0;
}


