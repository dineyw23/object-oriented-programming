#include<iostream>
#include<math.h>

struct Triangle 
{
	char color;
	char shade;
	short base;
  short height;
};

struct Circle 
{
	char color;
	char shade;
	short radius;
};

union fp
{
	void (*drawCircle_fp) (struct Circle*);
	float (*areaCircle_fp) (struct Circle*);
	void (*drawTriangle_fp) (struct Triangle*);
	float (*areaTriangle_fp) (struct Triangle*);
};

void setCircle(struct Circle* x,char c,char s,short r)
{
	x->color = c;
	x->shade = s;
	x->radius = r;
}

void setTriangle(struct Triangle* x,char c,char s,short b,short h)
{
	x->color = c;
	x->shade = s;	
  x->base = b;
	x->height = h;
}

void drawCircle(struct Circle* x)
{
	std::cout <<  "Circle ";
	if(x->shade == 'd')
		std::cout << "dark ";
	else
		std::cout << "light ";

	if(x->color == 'r')
		std::cout << "red ";
	else if(x->color == 'g')
		std::cout << "green ";
	else
		std::cout << "blue ";

	std::cout << "with radius = " << x->radius << std::endl;
}

void drawTriangle(struct Triangle* x)
{
	std::cout << "Triangle ";
	if(x->shade == 'd')
		std::cout << "dark ";
	else
		std::cout << "light ";

	if(x->color == 'r')
		std::cout << "red ";
	else if(x->color == 'g')
		std::cout << "green ";
	else
		std::cout << "blue ";
		
	std::cout << "with base = " << x->base << " height = " << 
	x->height <<std::endl;
}

float areaCircle(struct Circle* x)
{
	return((M_PI) * (x->radius) * (x->radius)); 
}

float areaTriangle(struct Triangle* x)
{
	return ((0.5) * (x->base) * (x->height));	
}


int main()
{
	struct Circle cObj;
	setCircle(&cObj,'b','d',12);

	struct Triangle tObj;
	setTriangle(&tObj,'r','l',4,6);

	union fp vtc[2];
	union fp vtt[2];

	vtc[0].drawCircle_fp = &drawCircle;
	vtc[1].areaCircle_fp = &areaCircle;
	vtt[0].drawTriangle_fp = &drawTriangle;
	vtt[1].areaTriangle_fp = &areaTriangle;	

	vtc[0].drawCircle_fp(&cObj);
	std::cout << "Area of Circle: " << vtc[1].areaCircle_fp(&cObj) 
	<< std::endl << std::endl;	
	vtt[0].drawTriangle_fp(&tObj);
	std::cout << "Area of Triangle: " << vtt[1].areaTriangle_fp(&tObj) 
	<< std::endl;	
	
	return 0;
} 

