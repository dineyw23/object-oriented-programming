#include <stdio.h>
float add(float a,float b)
{
	return (a + b);
}
float subtract(float a,float b)
{	
	return (a - b);
}

float multiply(float a,float b)
{
	return (a * b);
}

float divide(float a,float b)
{  
	if (a > b)
		return (a / b);
	else 
		return (b / a);
}

int main()
{	//Hard coded is fine.
	float a = 12.30;
	float b = 1.25;	
	
	float (*fpArray[4]) (float,float);
	fpArray[0] = &add;
	fpArray[1] = &subtract;
	fpArray[2] = &multiply;
	fpArray[3] = &divide;	
	
	for(int i = 0; i < 4; ++i)
	{
		float x = fpArray[i](a,b);	
		printf("Answer : %.3f\n",x);	
	}
	return 0;
}

