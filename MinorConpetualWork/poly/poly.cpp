#include<iostream>
#include<math.h>

//Using two fp
union fpCircle
{
	void (*drawCircle_fp) (struct Circle*);
	float (*areaCircle_fp) (struct Circle*);
};
union fpTrianlge
{
   void (*drawTriangle_fp) (struct Triangle*);
   float (*areaTriangle_fp) (struct Triangle*);
};

struct Triangle 
{
	char color;
	char shade;
   fpTrianlge vtt[2];	//Position matters
	short base;
   short height;
};

struct Circle 
{
	char color;
	char shade;
   fpCircle vtc[2];	//Position same as vtt
	short radius;
};


union shape_ptr 
{
  struct Circle* cir_ptr;
  struct Triangle* tri_ptr;
};

//Most imp

void do_poly(shape_ptr shape)
{
   //Only tri_ptr
	shape.tri_ptr->vtt[0].drawTriangle_fp(shape.tri_ptr); 
   std::cout << "Area: " << shape.tri_ptr->vtt[1].areaTriangle_fp(shape.tri_ptr) 
   << std::endl; 
}

void drawCircle(struct Circle* x)
{
	std::cout <<  "Circle ";
	if(x->shade == 'd')
		std::cout << "dark ";
	else
		std::cout << "light ";

	if(x->color == 'r')
		std::cout << "red ";
	else if(x->color == 'g')
		std::cout << "green ";
	else
		std::cout << "blue ";

	std::cout << "with radius = " << x->radius << std::endl;
}

void drawTriangle(struct Triangle* x)
{
	std::cout << "Triangle ";
	if(x->shade == 'd')
		std::cout << "dark ";
	else
		std::cout << "light ";

	if(x->color == 'r')
		std::cout << "red ";
	else if(x->color == 'g')
		std::cout << "green ";
	else
		std::cout << "blue ";
		
	std::cout << "with base = " << x->base << " height = " << 
	x->height <<std::endl;
}

float areaCircle(struct Circle* x)
{
	return((M_PI) * (x->radius) * (x->radius)); 
}

float areaTriangle(struct Triangle* x)
{
	return ((0.5) * (x->base) * (x->height));	
}
void setCircle(struct Circle* x,char c,char s,short r)
{
	x->color = c;
	x->vtc[0].drawCircle_fp = &drawCircle;
	x->vtc[1].areaCircle_fp = &areaCircle;	
	x->shade = s;
	x->radius = r;
}

void setTriangle(struct Triangle* x,char c,char s,short b,short h)
{
	x->color = c;
	x->shade = s;	
 	x->vtt[0].drawTriangle_fp = &drawTriangle;
	x->vtt[1].areaTriangle_fp = &areaTriangle;	
   x->base = b;
	x->height = h;
}
/*
	Polymorphic: The union shape_ptr has 2 struct pointers, when we are assigning
	the four objects to the shapes array, we are assigning first two to triangle 
	and second two to the circle ptr struct.refer [1][2]. When we are invoking the do_ poly 
	function, object 1 of triangle is passed and do_poly struct triangle will be 
	used. While for the last two iterations of the for loop we have assigned it 
	to cir_ptr and hence when do_poly is called it uses the cir_ptr struct instead
	of tri_ptr and then struct shape ptr points to circle struct instead of 
	triangle struct. Eventually, vtc is invoked and end up calling drawtriangle
	and areatriangle.
	Hence, do_poly shows a polymorphic behaviour where it calls appropriate
	struct according to what the shape_ptr is pointing to. We can have number of
	shapes and do_poly will work for all provided all struct have similar 
	declarations. shapes array's assignment is very important step here as 
	this will set the pointer to that particular shape according to what object
	is being passed.	

*/	
int main()
{
   shape_ptr shapes[4];
	
	struct Circle cObj1;
	struct Circle cObj2;
	setCircle(&cObj1,'g','l',6);
	setCircle(&cObj2,'b','d',12);

	struct Triangle tObj1;
	struct Triangle tObj2;
	setTriangle(&tObj1,'r','l',4,6);
	setTriangle(&tObj2,'b','d',6,13);
/*
   do_poly function just looks into the pointer to which the objects are assigned.
   Initially, tri_ptr is used and for last two index of shapes, it works accesses
   cir_ptr. The shape_ptr being a union, at a time it only considers one of its 
   item (Either tri_ptr or cir_ptr at a time.) So, last two items having cir objects
   assigned to cir_ptr, do_poly function considers accessing cir struct and not
   tri_ptr and uses VTC and calls drawCircle and areaCircle. 
 
 */

   //Very important to assign objects of same type to pointing shape.
	shapes[0].tri_ptr = &tObj1;//[1] 
	shapes[1].tri_ptr = &tObj2;//[1]
	shapes[2].cir_ptr = &cObj1;//[2]
	shapes[3].cir_ptr = &cObj2;//[2]

	for(int i = 0; i < 4; ++i)
 	{
		do_poly(shapes[i]);
   }

	return 0;
} 

