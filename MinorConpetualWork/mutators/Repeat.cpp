/**
 *  Repeat class : mutator
 */
#include <iostream>

using std::cout;
using std::ostream;
using std::endl;

class Repeat {
friend ostream& operator<<(ostream& os, const Repeat& rp);		
	public:
		Repeat(char c, int number) : character(c), count(number)
      { };
	private:
		int count;
		char character;
};

ostream& operator<<(ostream& os, const Repeat& rp)
{
	for(int i = 0;i < rp.count; ++i)
		os <<  rp.character;
	return os;
}

int main()
{
   cout <<  Repeat('-',80) << endl;
	return 0;
}		
