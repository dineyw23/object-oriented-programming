#include <iostream>
#include <string>

#include "StreamHandler.h"

int main(int argc, char* argv[])
{ 

  if(argc < 4)
  {
    std::cout << "Incomplete arguments" << std::endl;
    std::cout << "<olstring> <newstring> <filename>" << std::endl;
  }
  else if(argc > 4)
  {
    std::cout << "Extra arguments" << std::endl; 
    std::cout << "<olstring> <newstring> <filename>" << std::endl;
  }
  else
  {
    if(argv[1] != argv[2])
    {
      StreamHandler streamObj(static_cast<std::string> (argv[3]),
                              static_cast<std::string>(argv[1]),
                              static_cast<std::string>(argv[2]));
    }
  }
  return 0;
}
