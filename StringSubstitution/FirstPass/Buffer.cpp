#include "Buffer.h"

int Buffer :: buff_size()
{
   return q.size();
}

char& Buffer :: buff_front()
{
   return q.front();
}
void Buffer :: buff_pop()
{
   q.pop_front();
}

void Buffer :: buff_push(char& ch)
{
   q.push_back(ch);
}


