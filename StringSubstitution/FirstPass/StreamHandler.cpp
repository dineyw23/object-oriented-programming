#include <iostream>
#include <unistd.h>
#include "StreamHandler.h"

StreamHandler :: StreamHandler()
{
}

StreamHandler :: StreamHandler(std::string file,std::string olds,std::string news) 
:filename(file), getpos(0),putpos(0),match(-1),os(olds), ns(news)
{
   fileObj.open(filename.c_str(),std::ios::out | std::ios::in);
   if(fileObj.is_open())
    start();
  else
    std::cout << "Error opening file." << std::endl;
}

StreamHandler :: ~StreamHandler()
{
   fileObj.close();
   truncate(filename.c_str(),putpos);
}

void StreamHandler :: start()
{
  int x = ns.size() - os.size();
  if(x >= 0)
  {
    if(x > 0)
      newgreater();
    else
      samesize(); 
  }
  else
  {  
    oldgreater();
  }
}

void StreamHandler :: samesize()
{
   char ch;
   int i = 0;  
   do
   {
      if(check_eof())
         break;
      
      get(ch);
      
      if(getpos == -1)
        break;
      
      if(os[i] == ch) 
      {
         if(i == 0)
            match = static_cast<int>(getpos) - 1;
         putpos = match;
         
         if((getpos - putpos) == os.size()) 
         {
            
            for(int j = 0; j < ns.size(); j++)
            { 
               put(ns[j]);
            }      
            i = 0;
         }
         else
         {
            i++;
         }
      }     
      else 
      {
         if(getpos == -1)
          break;
         fileObj.clear(); 
         put(ch);
         i = 0;
         if(check_eof())
         {
            fileObj.setstate(std::ios::eofbit);
         }
      }      
    }while(check_eof() != true); 
}


void StreamHandler :: oldgreater()
{
   bool check = false;
   char ch;
   int temp;
   int i = 0;
   do 
   {
      get(ch);  
      
      if(getpos == -1) 
      {
         fileObj.clear(); 
         if((match != -1 && i == os.size()))
         {  
            if(i == 0)
              match = static_cast<int>(getpos) - 1;
           for(int j = 0;j < ns.size(); j++)
           { 
              put(ns[j]);   
           } 
            i = 0;
            match = -1;
         }
         break;
      }
      
      if(os[i] == ch)
      { 
         if(i == 0)
            match = static_cast<int>(getpos) - 1;
         
         if((match != -1 && i == os.size()))
         {  
            for(int j = 0;j < ns.size(); j++)
            { 
               put(ns[j]);   
            }  
            match = -1;
            i = 0;
         }
         else
         {
            i++;   
         }
      }
      else
      {
         if(check_eof())
         {
            if(getpos == -1)
            {
               fileObj.clear();
               put(ch);
               fileObj.setstate(std::ios::eofbit);
            }
         }
         else
         { 
            if(match == -1)
            {
               put(ch);
               i = 0;
            }
            else if(os.size() == i) 
            {
               for(int j = 0;j < ns.size(); j++)
               { 
                  put(ns[j]);   
                  if(j == ns.size() - 1)
                  {
                     i = 0;
                     match = -1;
                     break;    
                  }
               }
               if(ch != '\0')
                  put(ch); 
            }
            else if(os.size() != i) 
            { 
               int x = i;
               i = 0;

               for(int j = 0;j < x;j++)
               {
                  if(os[j + 1] == os[i] && ch  == os[i + 1])
                  {
                     put(os[j]); 
                     temp = j;
                     match = putpos;
                     check = true; 
                     i++;
                     i++;
                     break;
                  }
                  put(os[j]); 
                  
                  if(j == (x - 1))
                  {  
                     i = 0;
                     put(ch); 
                     match = -1;     
                     break;
                  }
               }
            }
         }
      }    
   }
   while(check_eof() != true);
}

void StreamHandler :: newgreater()
{
  char ch;
  char ch2;
  int i = 0;
  bool check = false;
  
  do
  {  
     if(fileObj.tellg() != -1)
        get(ch);
        
     if(buffObj.buff_size() != 0)
     {
        ch2 = ch;
        ch = buffObj.buff_front();
        buffObj.buff_pop();
        if(fileObj.tellg() != -1)
           buffObj.buff_push(ch2);
        if(buffObj.buff_size() == 0)
           check = true;
     }
     if(os[i] == ch)
     {
        i++;
        if(i == os.size())
        {
           newgreater1(ch,i,check);
           if(check == true)
              break;
        }
    }
    else 
    {
       fileObj.clear();
       put(ch);
       
       if(check == true)
          fileObj.setstate(std::ios::eofbit);
    }
  }while(!check_eof());
}

void StreamHandler :: newgreater1(char& ch,int& i,bool& check)
{
   for(int j = 0;j < (ns.size() - os.size());j++)
   {
      if(fileObj.tellg() != -1)
         get(ch);
      if(!fileObj.eof() && fileObj.tellg() != -1)
      {
         buffObj.buff_push(ch);
      }

      if(fileObj.eof() && buffObj.buff_size()  == 0)
      {
         check = true;
         fileObj.setstate(std::ios::eofbit);
      }
   }
   for(int j = 0;j < ns.size();j++)
   {
      fileObj.clear();
      put(ns[j]);
      if(buffObj.buff_size()== 0)
      {
         check = true;
         fileObj.setstate(std::ios::eofbit);  
      }
   }
   match = -1;
   i = 0;
}

char& StreamHandler :: get(char& ch)
{
   fileObj.seekg(getpos);
   fileObj.get(ch);
   
   if(fileObj.eof())
   {
      getpos = -1;
      return ch;
   }
   else
   {
      getpos = fileObj.tellg();
      return ch;   
   }       
}

void StreamHandler :: put(char& ch)
{
   
   if(!fileObj.eof())
   {
      fileObj.seekp(putpos);
      fileObj.put(ch);
      putpos = fileObj.tellp();
      return;
   }
   else
   {
      return;
   }
}

bool StreamHandler :: check_eof()
{
   if(fileObj.eof())
   {
      getpos = -1;
      return true;
   }
   else
      return false;
}
