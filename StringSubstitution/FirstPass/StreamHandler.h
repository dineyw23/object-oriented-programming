#ifndef STREAMHANDLER_H
#define STREAMHANDLER_H

#include <string>
#include <fstream>
#include "Buffer.h"

class StreamHandler : public std::fstream
{
  public:
  StreamHandler();
  StreamHandler(std::string,std::string,std::string);
  virtual ~StreamHandler();
  void newgreater();
  void newgreater1(char&,int&,bool&);
  void samesize();
  void oldgreater();
  void start();
  bool check_eof();
  virtual char& get(char&);
  virtual void put(char&);   
 
  private: 
  std::string filename;
  std::fstream fileObj;
  std::string os;
  std::string ns;
  std::streampos getpos;
  std::streampos putpos;  
  std::streampos match;
  Buffer buffObj;
};

#endif
