#ifndef BUFFER_H
#define BUFFER_H

#include "TrackingDeque.h"

class Buffer 
{
public:
   int buff_size();
   char& buff_front();
   void buff_pop();
   void buff_push(char& ch);
   
private:
   TrackingDeque<char> q;
};
#endif
