/*:
 *  @author: Diney Wankhede
 *  @file: stampbuf.cpp
 */

#include "stampbuf.h"
#include <stdio.h>
#include <ctype.h> 
#include <cstring>

stampbuf :: stampbuf(int c, int r) : cur_col(0),cur_row(0), col_stream(c), 
row_stream(r) , char_array(new char[c/2])
{
	//Set pbase and epptr & pptr is set automatically to pbase
	setp(char_array, (char_array+ 1)); 
  //void setp (char* new_pbase, char* new_epptr) 
  try {
    stamping_press :: insert_plate(c,r);}
  catch(...)
  { }
}

stampbuf :: ~stampbuf()
{
  sync();
  try {
  stamping_press :: eject_plate(); }
  catch(...)
  {}   
  delete[] char_array;
}

int stampbuf :: sync() //Returns 0 -> success && -1 -> failure
{
	char current;
	int size;	
  size = pptr() - pbase(); 
	for(int i = 0; i < size; ++i)
	{
	  current = char_array[i];
    if(current != stamping_press :: get_die()) 
    {
      if(checks_set_die(current))
		    stamping_press :: set_die(current);
      else  
        ++cur_col;
    }
 		if(checks_stamp(current)) 
    {
  		stamping_press :: stamp(cur_col,cur_row);
		    ++cur_col;
    }
  }
  return 0;
}	


int stampbuf :: overflow(int c)//int overflow (int c = EOF);
{
  if(char(c) != EOF)
  {
    sync();
    char current = c; //Just like sync, only one char
    if(current != stamping_press :: get_die()) 
    {
      if( checks_set_die(current))
		    stamping_press :: set_die(current);
      else
        ++cur_col;
    }
 		if(checks_stamp(current)) 
    {
  		stamping_press :: stamp(cur_col,cur_row);
		    ++cur_col;
    }
  }      
  setp(char_array,(char_array + (col_stream/2)));
  return c;
}	


bool stampbuf :: checks_set_die(const char& current) const
{
  if(current != stamping_press::get_die())
	{
		if(!isspace(current) && current != '\n')
		{ 
			if((isdigit(current) || isalpha(current) || current == '*' || current == '#'))
			{
	 			return true;
			}
		}
	}
	return false;
}


bool stampbuf :: checks_stamp(const char& current) const
{
	if(cur_col < col_stream && cur_row < row_stream)
	{
		if(!isspace(current) && current != '\n')
		{
			if((isdigit(current) || isalpha(current) || current == '*' || current == '#'))
				return true;
		}
	}
	return false;
}

void stampbuf :: call_endrow()
{
  sync();
  setp(char_array,(char_array + (col_stream/2)));
  cur_row++;
  cur_col = 0;
}

void stampbuf :: move_to_row(int row_change)
{
  sync();
  setp(char_array,(char_array + (col_stream/2)));
  cur_col = 0;
  cur_row = row_change;
}
