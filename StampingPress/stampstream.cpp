/*
 *  @author: Diney Wankhede
 *  @file: stampstream.cpp
 */

#include "stampstream.h"

stampstream :: stampstream(int c,int r) : std::ostream(new stampbuf(c,r)) 
{
	//Nothing: Initialization list  
}
stampstream :: ~stampstream()
{
  delete rdbuf();	//
}

//Initialization List
row::row(int r) : go_to_row(r){ }

int row :: getter() const
{
  return go_to_row;
}

std::ostream& endrow(std::ostream& os)
{
  stampbuf* array_pointer = static_cast <stampbuf*>(os.rdbuf()); 
	array_pointer -> call_endrow();
  return os;
}

std::ostream& operator<<(std::ostream& os, const row& r)
{
   stampbuf* array_pointer = static_cast <stampbuf*>(os.rdbuf()); 
   int x = r.getter();
   array_pointer -> move_to_row(x);
   return os; 
}


