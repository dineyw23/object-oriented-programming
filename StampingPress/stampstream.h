#ifndef STAMPSTREAM_H
#define STAMPSTREAM_H

#include <ostream>
#include "stampbuf.h"
#include "stamp.h"

class row
{
  public:
	row(int r);
	int getter() const;
  
  private:  
	int go_to_row;
};

//Inerited from std::ostream
class stampstream : public std::ostream 
{
  public:
	stampstream(int c,int r);
	~stampstream();
};

std::ostream& operator<<(std::ostream& os,const row& r);
std::ostream& endrow(std::ostream& os);

#endif


