#ifndef STAMPBUF_H
#define STAMPBUF_H

#include <iostream>
#include "stamp.h"

class stampbuf : public std::streambuf
{
	public:
	stampbuf(int c, int r);
	~stampbuf();
	virtual int sync();         //Overriden
	virtual int overflow(int c);//Overriden
  void move_to_row(int);
  void call_endrow();

	private:
	int cur_col;
	int cur_row;
	int col_stream; //Max
	int row_stream; //Max
  char* char_array; //Buffer
  
  //To implement cleaner sync and overflow
  bool checks_set_die(const char&) const;
  bool checks_stamp(const char&) const;
};

#endif
