#include <iostream>
#include "final.h"
using namespace std;
int main()
{
   
   /*
	Stack <int> intStack;
	Stack <string> stringStack;

	      intStack.push(7); 
        cout << intStack.pop() <<endl; 
	try
	{
        // manipulate string stack 
        stringStack.push("hello"); 
        cout << stringStack.size() << std::endl; 
      cout << stringStack.pop() << endl;
       //cout << stringStack.pop() <<endl; 
    } 
    catch (std::exception const& ex) { 
       std::cerr << "Exception: " << ex.what() <<endl; 
        return 0;
}
//	return 0;
    */
   Stack<float> s;
   for(int i=0; i<100; ++i)
      s.push(i*3.14);
   cout << s.size() << "=100?" << endl;
   s.pop();
   s.pop();
   s.pop();
   cout << s.size() << "=97?" << endl;
   Stack<float> t;
   t=s;
   cout << s.size() << "=97?" << endl;
   
   return 0;

}
