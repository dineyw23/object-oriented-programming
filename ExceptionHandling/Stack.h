/**
 *	Stack class as template with proper Exception Handling.
**/

template <typename T>
class Stack 
{
public:
	Stack();
	Stack(const Stack&);
	~Stack();
   Stack& operator=(const Stack&);
	int size();
	void push(T);
	T pop();			
private:
	static const int chunk_size;
   unsigned maximum_size;
   int current_size;   
	T* base; 
};

template <typename T>
const int Stack<T>::chunk_size = 10;


//Default Cstor
template <typename T>
Stack<T>::Stack(): current_size(-1),maximum_size(chunk_size),
									base(new T[chunk_size])
{
   
}


//Copy Cstor
template <typename T>
Stack<T>::Stack(const Stack& s)
:maximum_size(s.maximum_size), base(new T[s.maximum_size])
{
   if(s.current_size > -1)
   {
      try {
 	   for(current_size=0; current_size<=s.current_size; current_size++)
         base[current_size] = s.base[current_size];   //Copy cstor
      }
      catch(...)	
      {
         delete [] base; //Releasing memory before throw		
         throw;	
      }
      current_size--;
   }
}

//Never ever throw any exception in desctructor.
template <typename T>
Stack<T>::~Stack()
{
  delete [] base;
}



//Assignment operator
template<typename T>
Stack<T>& Stack<T>::operator=(const Stack<T>& s)
{	
   T* temp_base =  NULL;		//Storing the base to delete later
   temp_base = base;
  	if(temp_base != NULL)
  	{
  		int temp_size = maximum_size;
  		maximum_size = s.maximum_size;
  		try
   		{
            base=new T[maximum_size]; //default
   		}	
   		catch(...)
   		{   
             maximum_size = temp_size; //If it fails above,no mem is allocated.
             base = temp_base;
             throw;
   		} 
			
         int temp_current_size = current_size;
         current_size = s.current_size;
         if(current_size>-1)
    		{
         
 		 		try
				{  
               for(int x=0; x<=current_size; x++)
                  base[x] = s.base[x];  //Assignment operator definately       
            }
				catch(...)
				{
					delete [] base;
					base = temp_base;
               current_size = temp_current_size;
					throw;	
				}
			}
  	 
    }
   if(temp_base != NULL)
     delete [] temp_base;
  return *this;
}


//The most difficult one.
template <typename T>
void Stack<T> :: push(T  element)
{		
   //Not incrementing initially 
   if(current_size + 1 == maximum_size-1) // Stack almost full --> ??
   { 
      int temp_max_size = maximum_size;
      maximum_size += chunk_size;
       
      T* new_buffer = NULL;
       
      try{
          new_buffer = new T[maximum_size];  //default
      }
      catch(...)
      {
         maximum_size = temp_max_size;
         throw;  
      }
      
      if(new_buffer != NULL)
      {
         try {
            for(int x=0; x < current_size+1; x++)
               new_buffer[x] = base[x];		//Assignment operator
         } catch (...) {
            //No decrement 
            throw;
         }
         
      }
      if (new_buffer != NULL) 
      {
         T* base_0_temp = base;
         delete [] base;
         try{
				 base = new_buffer;  //Member may change
         }
         catch(...){
            //No decrement 
            base = base_0_temp;
            throw;
         }
      }
	} 
   T* base_temp = base;
   try{
      base[current_size+1] = element;//Assignment operator, compulsarily called
      current_size++;  //Increment only if assignment is successful
   }
   catch(...)
   {
      base = base_temp;  //Original
      throw;  //current_size is not changed, initially. So no decrement.     
   }
}

//Din't expect this to throw an exception. But this calls COPY CONSTRUCTOR.
template <typename T>
T Stack<T>::pop()
{
   //  if(current_size < 0)        //I may keep this, but not required
    //    throw "Nothing to pop";
   try 
   { //Copy cstor gets called - Return by value
      return base[current_size--];
   } 
   catch (...) 
   { //pop failed. Roll back to original size
      current_size ++;
      throw;
   }
}

//Generic is not used/related. Safe. 
template <typename T>
int Stack<T> :: size()
{
   if(current_size >= -1)
		 return current_size + 1;
   else
      return 0;
}

