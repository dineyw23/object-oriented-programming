#ifndef PERIODIC_H
#define PERIODIC_H

#include <string>

#include "Media.h"

class Periodic : public Media {
private:
   std::string m_author;
   std::string m_desc;
   std::string m_pub;
   std::string m_pub_history;
   std::string m_related_titles;
   std::string m_series;
   std::string m_other_titles;
   std::string m_govt_no;

   void display() const;
   bool cmp_other(const std::string&) const;
   
public:
   
   Periodic(const std::string& call_no,const std::string& title, 
            const std::string& subjects,const std::string& author,
            const std::string& desc,const std::string& pub,
            const std::string& pub_history,const std::string& series,
            const std::string& notes,const std::string& related_titles,
            const std::string& other_titles,const std::string& govt_no);
};

#endif
