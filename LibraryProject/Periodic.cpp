#include <iomanip>
#include <iostream>

#include "Periodic.h"

Periodic :: Periodic(const std::string& call_no,const std::string& title, 
                     const std::string& subjects,
                     const std::string& author,const std::string& desc,
                     const std::string& pub,const std::string& pub_history,
                     const std::string& series, const std::string& notes,
                     const std::string& related_titles,
                     const std::string& other_titles,
                     const std::string& govt_no):
Media(call_no,title,subjects,notes), m_author(author), m_desc(desc),m_pub(pub),
m_pub_history(pub_history),m_series(series),m_related_titles(related_titles),
m_other_titles(other_titles),m_govt_no(govt_no) 
{
   
}

void Periodic :: display() const
{
   std::cout << "=======================Search Result=========================="
   << std::endl;
   std::cout << std::left << std::setw(15)<< "Media type:    Periodical" 
   << std::endl;
   Media :: display();
   std::cout << std::setw(15) << "Author: " << m_author << std::endl;
   std::cout << std::setw(15) << "Description: " << m_desc << std::endl;
   std::cout << std::setw(15) << "Publisher: " << m_pub << std::endl;
   std::cout << std::setw(15) << "Publisher History:" << m_pub_history 
   << std::endl;
   std::cout << std::setw(15) << "Series: " << m_series << std::endl;
   std::cout << std::setw(15) << "Related Titles:" << m_related_titles 
   << std::endl;
   std::cout << std::setw(15) << "Other Titles: " << m_other_titles 
   << std::endl;
   std::cout << std::setw(15) << "Govt. Doc No: " << m_govt_no << std::endl;
   
   
   
}

bool Periodic :: cmp_other(const std::string& search_other) const
{
   std::size_t found = m_desc.find(search_other);
   std::size_t found2 = m_notes.find(search_other);
   std::size_t found3 = m_series.find(search_other);
   std::size_t found4 = m_related_titles.find(search_other);
   
   if(found == std::string::npos && found2 == std::string::npos
      && found3 == std::string::npos && found4 == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }
}