#include <iomanip>
#include <iostream>

#include "Media.h"

Media :: Media(const std::string& call_no,
               const std::string& title,
               const std::string& subjects,
               const std::string& notes) : m_call_no(call_no),m_title(title),
                           m_subjects(subjects), m_notes(notes)
{
   //Media: 4 arg cstr   
}

Media :: ~Media()
{
   //Media: dtor
}

void Media :: display() const
{
   std::cout << std::left 
             << std::setw(15) << "Call number: " << m_call_no << std::endl;
   std::cout << std::setw(15) << "Title: " << m_title << std::endl;
   std::cout << std::setw(15) << "Subjects: " << m_subjects << std::endl;
   std::cout << std::setw(15) << "Notes: " << m_notes << std::endl;

}
 
bool Media::cmp_title(const std::string& search_title) const
{
   std::size_t found =  m_title.find(search_title);
   if(found == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }
}

bool Media :: cmp_cn(const std::string& search_string) const
{
   std::size_t found = m_call_no.find(search_string);
   if(found == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }

}
 
bool Media :: cmp_subject(const std::string& search_string) const
{
   std::size_t found = m_subjects.find(search_string);
   if(found == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }

}
