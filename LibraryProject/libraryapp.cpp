#include <iostream>

#include "SearchEngine.h"

int main()
{
	SearchEngine searcheng;
	
   std::string type;
   std::string search_string;
   bool end = false;
   
   while (end == false)
   {
      
      std::cout
      << "=============================================================="
      << std::endl;
      std::cout << "Enter the type of search, please:" << std::endl;
      std::cout
      << "=============================================================="
      << std::endl;
      std::cout << "Search by:" << std::endl;
      std::cout << "Title" << std::endl;
      std::cout << "Call number" << std::endl;
      std::cout << "Subject" << std::endl;
      std::cout << "Other" << std::endl;
      std::cout << "To quit: Exit" << std::endl;
      std::cout
      << "=============================================================="
      << std::endl;

      std::getline(std::cin,type);
      
      if(type == "Exit")
      {
         end = true;
      }
      if(type == "Title" || type == "Call number" || type == "Subject" || 
         type == "Other")
      {
         std::cout
         << "=============================================================="
         << std::endl;
         std::cout << "Enter the string that is to be searched:" << std::endl;
         std::cout
         << "=============================================================="
         << std::endl;

         std::getline(std::cin,search_string);
         
         if (!type.empty() && !search_string.empty()) 
         {
            std::vector<Media*> final;
            
            if (type == "Title")
            {
               final = searcheng.search_title(search_string);  
            }
            else if (type == "Call number")
            {
               final = searcheng.search_call_no(search_string);  
            }
            else if(type == "Subject")
            {
               final = searcheng.search_subject(search_string);  
            }
            else if(type == "Other")
            {
               final = searcheng.search_other(search_string);  
            }
            
            if (!final.empty()) 
            {
               std::cout
               <<"=============================================================="
               << std::endl;
               std::cout << final.size() << " entries found that matches '" 
               << search_string << "' when searched by " << type << "." 
               << std::endl;
               
               for (std::vector<Media*>::iterator it = final.begin(); 
                    it != final.end(); ++it)
               {
                  (*it)->display();
               }
               
               std::cout
               <<"=============================================================="
               << std::endl;
               std::cout << final.size() << " entries found that matches '" 
               << search_string << "' when searched by " << type << "." 
               << std::endl;
            }
            else
            {
               std::cout
               <<"=============================================================="
               << std::endl;
               std::cout << "No entry matching the search string." << std::endl;

            }
         }
      }
      else
      {
         if (end == true) 
         {
            std::cout
            << "=============================================================="
            << std::endl;
            std::cout << "Exiting the Library application." << std::endl;
            std::cout
            << "=============================================================="
            << std::endl;

         }
         else
         {
            std::cout
            << "=============================================================="
            << std::endl;
            std::cout << "Not a valid search." << std::endl; 
   
         }
      }
   }
   return 0;
}

