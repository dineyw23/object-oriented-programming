#ifndef FILM_H
#define FILM_H

#include <string>
#include "Media.h"

class Film : public Media {
private:

   std::string m_director;
   std::string m_year;
   
   void display() const;
   bool cmp_other(const std::string&) const;

public:
   Film(const std::string& call_no, const std::string& title,
        const std::string& subjects, const std::string& director,
        const std::string& notes,const std::string& year);
};

#endif
