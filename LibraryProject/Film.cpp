#include <iomanip>
#include <iostream>

#include "Film.h"

Film :: Film (const std::string& call_no, const std::string& title,
              const std::string& subjects, const std::string& director,
              const std::string& notes, const std::string& year) :
Media(call_no,title,subjects,notes), m_director(director), m_year(year)
{
   // Film: 6 cstor
}

void Film :: display() const
{
   std::cout << "=======================Search Result=========================="
   << std::endl;
   std::cout << std::left << std::setw(15) << "Media type:    Film" 
   << std::endl;
   Media :: display();
   std::cout << std::setw(15) << "Director: " << m_director 
   << std::endl;
   std::cout << std::setw(15) << "Year: " << m_year << std::endl;
}

bool Film :: cmp_other(const std::string& search_other) const
{
   std::size_t found = m_notes.find(search_other);
   std::size_t found2 = m_director.find(search_other);
   std::size_t found3 = m_year.find(search_other);
   
   if(found == std::string::npos && found2 == std::string::npos &&
      found3 == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }

}