#include<iomanip>
#include<fstream>
#include<iostream>

#include "SearchEngine.h"
#include "Book.h"
#include "Periodic.h"
#include "Video.h"
#include "Film.h"

SearchEngine::SearchEngine()
{
   std::string call_no;
   std::ifstream inf("book.txt");
   if (!inf.is_open())
   {
      std::cout << "Error reading from file." << std::endl;
   }
   else
   {
      while (!std::getline(inf,call_no,'|').eof())
      {
      
         std::string title, subjects, author, desc,
         pub, city,  year, series, notes;
         std::getline(inf,title,'|');
         std::getline(inf,subjects,'|');
         std::getline(inf,author,'|');
         std::getline(inf,desc,'|');
         std::getline(inf,pub,'|');
         std::getline(inf,city,'|');
         std::getline(inf,year,'|');
         std::getline(inf,series,'|');
         std::getline(inf,notes);
         
         CardCatalog.push_back(new Book(call_no,title,subjects,author,desc,
                                        pub,city,year,series,notes));
      }             
      inf.close();           
   
   
   inf.open("periodic.txt");
   if (!inf.is_open())
   {
      std::cout << "Error reading from file." << std::endl;
   }
   else
   {
      while (!std::getline(inf,call_no,'|').eof()) 
      {
         std::string title, subjects, author, desc,
         pub, pub_history,  series, notes, related_titles,other_titles,govt_no;
         std::getline(inf,title,'|');
         std::getline(inf,subjects,'|');
         std::getline(inf,author,'|');
         std::getline(inf,desc,'|');
         std::getline(inf,pub,'|');
         std::getline(inf,pub_history,'|');
         std::getline(inf,series,'|');
         std::getline(inf,notes,'|');
         std::getline(inf,related_titles,'|');
         std::getline(inf,other_titles,'|');
         std::getline(inf,govt_no);
      
         CardCatalog.push_back(new Periodic(call_no,title,subjects,author,desc,
                                    pub,pub_history,series,notes,related_titles,
                                    other_titles,govt_no));
      }
      inf.close();
   }
  
   inf.close();
   
   inf.open("video.txt");
   if (!inf.is_open())
   {
      std::cout << "Error reading from file." << std::endl;
   }
   else
   {
      while (!std::getline(inf,call_no,'|').eof()) 
      {
         std::string title, subjects, desc,
         distributor, notes, series , label;
         std::getline(inf,title,'|');
         std::getline(inf,subjects,'|');
         std::getline(inf,desc,'|');
         std::getline(inf,distributor,'|');
         std::getline(inf,notes,'|');
         std::getline(inf,series,'|');
         std::getline(inf,label);
         
         CardCatalog.push_back(new Video(call_no,title,subjects,desc,
                                         distributor,notes,series,label));
      }
      inf.close();
   }

   
   
   inf.open("film.txt");
   if (!inf.is_open())
   {
      std::cout << "Error reading from file." << std::endl;
   }
   else
   {
      while (!std::getline(inf,call_no,'|').eof())
      {
      
         std::string title, subjects, director,notes , year;
         std::getline(inf,title,'|');
         std::getline(inf,subjects,'|');
         std::getline(inf,director,'|');
         std::getline(inf,notes,'|');
         std::getline(inf,year);
         
         CardCatalog.push_back(new Film(call_no, title, subjects, director,
                                     notes , year));
      }
      inf.close();
   }
}

SearchEngine::~SearchEngine()
{
   for (std::vector<Media*>::iterator it = CardCatalog.begin();
        it != CardCatalog.end(); ++it) 
   {
      delete (*it);
   }
}

std::vector<Media*> 
SearchEngine :: search_title(const std::string& search_title) const
{
   std::vector<Media*> result_list;
   for (int i = 0; i < CardCatalog.size(); i++)
   {
      if(CardCatalog[i]->cmp_title(search_title))
      {
         result_list.push_back(CardCatalog[i]);
      }
   }
   return result_list;
}
 
std::vector<Media*> 
SearchEngine :: search_call_no(const std::string& search_call_no) const
{
   std::vector <Media*> result_list;
   if (result_list.empty()) 
   {
      for (int i = 0; i < CardCatalog.size(); i++)
      {
         if (CardCatalog[i]->cmp_cn(search_call_no))
         {
            result_list.push_back(CardCatalog[i]);
         }
      }
   }
   return result_list;
}

std::vector<Media*> 
SearchEngine :: search_subject(const std::string& search_subject) const
{
   std::vector<Media*> result_list;
   for (int i = 0; i < CardCatalog.size(); i++)
   {
      if(CardCatalog[i]->cmp_subject(search_subject))
      {
         result_list.push_back(CardCatalog[i]);
      }
   }
   return result_list;
}


std::vector<Media*>
SearchEngine :: search_other(const std::string& search_other) const
{
   std::vector<Media*> result_list;
   for (int i = 0; i < CardCatalog.size(); i++)
   {
     if(CardCatalog[i]->cmp_other(search_other))
      {
         result_list.push_back(CardCatalog[i]);
      }
   }
   return result_list;
}
