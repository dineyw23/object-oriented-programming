#ifndef BOOK_H
#define BOOK_H

#include "Media.h"
#include <string>

class Book : public Media {
private:
   std::string m_author;
   std::string m_desc;
   std::string m_publisher;
   std::string m_city;
   std::string m_year;
   std::string m_series;
   bool cmp_other(const std::string&) const;
   void display() const;

public:
   Book(const std::string& call_no,const std::string& title,
        const std::string& subjects,const std::string& author,
        const std::string& desc,const std::string& pub,
        const std::string& city, const std::string& year,
        const std::string& series, const std::string& notes); 
};

#endif
