#include <iomanip>
#include <iostream>

#include "Book.h"

Book :: Book(const std::string& call_no, const std::string& title, 
             const std::string& subjects,const std::string& author,
             const std::string& desc,const std::string& pub,
             const std::string& city, const std::string& year,
             const std::string& series,const std::string& notes): 
     Media(call_no,title,subjects,notes) , m_author(author),m_desc(desc),
     m_publisher(pub),m_city(city),m_year(year),m_series(series)

{
   //Book: cstor
}

void Book :: display() const
{
   std::cout << "=======================Search Result=========================="
   << std::endl;
 
   std::cout << "Media type:    Book" << std::endl;
   
   Media :: display();
   
   std::cout << std::left 
             << std::setw(15) << "Author: " << m_author << std::endl;
   std::cout << std::setw(15) << "Description: " << m_desc << std::endl;
   std::cout << std::setw(15) << "Publisher: " << m_publisher << std::endl;
   std::cout << std::setw(15) << "City: " << m_city << std::endl;
   std::cout << std::setw(15) << "Year: " << m_year << std::endl;
   std::cout << std::setw(15) << "Series: " << m_series << std::endl;
}

bool Book :: cmp_other(const std::string& search_other) const
{
   std::size_t found = m_desc.find(search_other);
   std::size_t found2 = m_notes.find(search_other);
   std::size_t found3 = m_year.find(search_other);
   
   if(found == std::string::npos && found2 == std::string::npos
      && found3 == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }
}