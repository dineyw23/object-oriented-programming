#include <iomanip>
#include <iostream>

#include "Video.h"

Video :: Video(const std::string& call_no,const std::string& title,const std::string& subjects,
               const std::string& desc,const std::string& distributor, const std::string& notes,
               const std::string& series,const std::string& label) :
Media(call_no,title,subjects,notes), m_desc(desc), m_distributor(distributor),
m_series(series),m_label(label)

{
   //Video: cstor  
}
                                            
void Video :: display() const
{
   std::cout << "=======================Search Result=========================="
   << std::endl;
   std::cout << std::left << std::setw(15) << "Media Type:    Video" 
   << std::endl;
   Media :: display();
   std::cout << std::setw(15) << "Description: " << m_desc << std::endl;
   std::cout << std::setw(15) << "Distributor: " << m_distributor << std::endl;
   std::cout << std::setw(15) << "Series: " << m_series << std::endl;
   std::cout << std::setw(15) << "Label: " << m_label << std::endl;
}

bool Video :: cmp_other(const std::string& search_other) const
{
   std::size_t found = m_desc.find(search_other);
   std::size_t found2 = m_notes.find(search_other);
   std::size_t found3 = m_distributor.find(search_other);
   
   if(found == std::string::npos && found2 == std::string::npos
      && found3 == std::string::npos)
   {
      return false;
   }
   else
   {
      return true;
   }
}