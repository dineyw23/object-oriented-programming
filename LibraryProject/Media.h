#ifndef MEDIA_H
#define MEDIA_H

#include<string>

class Media {
public:
   Media(const std::string& call_no,
         const std::string& title,
         const std::string& subjects,
         const std::string& notes);
   virtual ~Media();
   
   bool cmp_cn(const std::string&) const;
   bool cmp_title(const std::string&) const;
   bool cmp_subject(const std::string&) const;
   virtual void display() const;
   virtual bool cmp_other(const std::string&) const = 0;
   
private:
   std::string m_call_no;
   std::string m_title;
   std::string m_subjects;

protected:
   std::string m_notes;
};

#endif