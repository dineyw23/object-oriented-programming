#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

#include<vector>
#include<string>

#include "Media.h"

class SearchEngine {
public:
   SearchEngine();
   ~SearchEngine();
  
   std::vector<Media*> search_title(const std::string&) const;
   std::vector<Media*> search_call_no(const std::string&) const;
   std::vector<Media*> search_subject(const std::string&) const;
   std::vector<Media*> search_other(const std::string&) const;
   
private:
   std::vector<Media*> CardCatalog;
};

#endif
