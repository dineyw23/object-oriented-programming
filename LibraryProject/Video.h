#ifndef VIDEO_H
#define VIDEO_H

#include <string>
#include "Media.h"

class Video : public Media {
private:
   std::string m_distributor;
   std::string m_desc;
   std::string m_label;
   std::string m_series;
   
   bool cmp_other(const std::string&) const;
   void display() const;

public:
   Video(const std::string& call_no,const std::string& title,
         const std::string& subjects,const std::string& desc,
         const std::string& distributor, const std::string& notes,
         const std::string& series,const std::string& label);

};

#endif
